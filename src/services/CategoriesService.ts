import { CategoryModel } from "@/interfaces/interfaces"

class CategoriesService {
    async GetAllCaegories(): Promise<Array<CategoryModel>> {
        const response = await fetch("https://localhost:44305/api/category", {
            method: "GET"
        });

        return response.ok ? response.json() : null;
    }
}

export default CategoriesService;