import { Item, AddItemModel } from "@/interfaces/interfaces"

class ItemService {
    async GetAllItems(): Promise<Array<Item>> {
        const response = await fetch("https://localhost:44305/api/item", {
            method: "GET"
        });

        return response.ok ? response.json() : null;
    }

    async AddItem(newItem: AddItemModel): Promise<boolean> {
        const response = await fetch("https://localhost:44305/api/item", {
            headers: {
                "Content-Type": "application/json"
              },
            method: "POST",
            body: JSON.stringify(newItem)
        });

        return response.ok;
    }
}

export default ItemService;