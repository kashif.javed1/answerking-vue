export default class Utilities {

    public ToTwoDecimal(num: number) {
        return num.toFixed(2);
    }

    public CapitaliseString(string: string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}