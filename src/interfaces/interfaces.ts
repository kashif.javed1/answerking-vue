export interface Item {
    id: number;
    name: string;
    unitPrice: number;
}

export interface AddItemModel {
    name: string;
    unitPrice: number;
}

export interface CategoryModel {
    id: number;
    name: string;
    items: Array<Item>;
}