import Vue        from "vue";
import App        from "./App.vue";
import router     from "./router";
import Antd       from "ant-design-vue";
import                 "ant-design-vue/dist/antd.css";
import Utilities  from "@/utilities/Utilities";

const utilities = new Utilities();

Vue.filter("ToTwoDecimal", utilities.ToTwoDecimal);
Vue.filter("Capitalise", utilities.CapitaliseString);
Vue.config.productionTip = false;
Vue.use(Antd);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
