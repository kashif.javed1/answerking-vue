import Vue                          from "vue";
import VueRouter, { RouteConfig }   from "vue-router";
import Home                         from "@/views/Home.vue";
import ViewItems                    from "@/views/ViewItems.vue";
import AddItem                      from "@/views/AddItem.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/view-items",
    name: "ViewItems",
    component: ViewItems
  },
  {
    path: "/add-item",
    name: "AddItem",
    component: AddItem
  }
];

const router = new VueRouter({
  routes
});

export default router;
